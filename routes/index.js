var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index.ejs', 
  { title: 'SSCRA',
    layout: 'layout.ejs'  });
});

/* GET Report page. */
router.get('/report', function(req, res, next) {
  res.render('report.ejs', 
  { title: 'SSCRA',
    layout: 'layout.ejs'  });
});
module.exports = router;
